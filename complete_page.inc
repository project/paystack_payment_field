<?php

function complete_page(){
  if(!$_POST){
    drupal_goto(variable_get('front_page'));
  
  }else{
    $reference = check_plain($_POST['reference']);
    $secret_key = variable_get('paystack_payment_field_secret_key');

    $uid = check_plain($_POST['uid']);
    $ent_id = check_plain($_POST['ent_id']);
    $entity_type = check_plain($_POST['ent_type']);

    $url = 'https://api.paystack.co/transaction/verify/'.$reference;
    $options = array(
      'method' => 'GET',
      'headers' => array('Authorization' => 'Bearer '.$secret_key),
    );
    $request = drupal_http_request($url, $options);
    if ($request) {
      $result = json_decode($request->data, true);
      //dpm($result);
      if($result){
        if($result['data']){
          //something came in
          if($result['data']['status'] == 'success'){
            $amount = ($result['data']['amount']) / 100;
            $date = strtotime($result['data']['transaction_date']);
            
            // Trigger the rules event.
            $entity_wrapper = entity_metadata_wrapper($entity_type, $ent_id);
            rules_invoke_all('paystack_payment_field_successful', $ent_id, $entity_wrapper, $uid, $amount, $reference, $date);
            
            return '';
           // header("Location: " . $base_url . "/" . $location);
          //  exit();

          }else{
            return  "Transaction was not successful: Last gateway response was: ".$result['data']['gateway_response'];
          }
        }else{
          return $result['message'];
        }

      }else{
        //print_r($result);
        return "Something went wrong while trying to convert the request variable to json.";
      }
    }else{
      //var_dump($request);
      return "Something went wrong while executing curl.";
    }
    

  }
  
}