<?php 

/**
 * @file
 * Rules integration for the paystack_payment_field module.
 */


/**
 * Implements hook_rules_event_info().
 *
 * @return array
 */
function paystack_payment_field_rules_event_info() {
  return array(
  	'paystack_payment_field_successful' => array(
      'label' => t('Payment is successful'),
      'group' => t('Button Field'),
      'variables' => array(
        'entity_id' => array(
          'type' => 'integer',
          'label' => t('Entity ID'),
          'description' => t('Entity ID from which the Payment Button field was clicked.')
        ),
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity'),
          'description' => t('Entity from the button that was clicked.'),
        ),
        'uid' => array(
          'type' => 'integer',
          'label' => t('User ID'),
          'description' => t('The user ID of the payer'),
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Amount Paid'),
          'description' => t('Amount Paid'),
        ),
        'trnx_reference' => array(
          'type' => 'text',
          'label' => t('Transaction Reference'),
          'description' => t('Transaction Reference ID'),
        ),
        'date' => array(
          'type' => 'date',
          'label' => t('Transaction Date'),
          'description' => t('Transaction Date'),
        ),
      ),
    ),
  );
}

