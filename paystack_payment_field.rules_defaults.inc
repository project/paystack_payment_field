<?php 

/**
 * Implements hook_rules_defaults().
 */
function paystack_payment_field_default_rules_configuration() {
  $data = array();
  
  $data['paystack_payment_field_successful'] = rules_import( 
    '{ "paystack_payment_field_successful" : {
      "LABEL" : "After Payment is successful",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : true,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : { "paystack_payment_field_successful" : [] },
      "DO" : [
         { "drupal_message" : { "message" : "Payment of [amount:value] successful on the entity ID: [entity-id:value] by [uid:value] on [date:value]" } },
         { "redirect" : { "url" : [ "site:url" ] } }
      ]
    }
    }'
    
  );
  
  return $data;
}

