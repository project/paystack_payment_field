<?php 

function _webform_defaults_paystack_payment_field() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'field_prefix' => '',
      'field_suffix' => '',
    ),
  );
}

function _webform_theme_paystack_payment_field() {
  return array(
    'webform_display_paystack_payment_field' => array(
      'render element' => 'element',
    ),
  );
}

function _webform_edit_paystack_payment_field($component) {
  $form = array();
  $form['extra']['field_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Left'),
    '#default_value' => $component['extra']['field_prefix'],
    '#size' => 60,
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => 1.1,
    '#parents' => array('extra', 'field_prefix'),
    '#description' => t('Text placed to the left of the paystack_payment_field'),
  );
  $form['extra']['field_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Right'),
    '#default_value' => $component['extra']['field_suffix'],
    '#size' => 60,
    '#required' => TRUE,
    '#maxlength' => 255,
    '#weight' => 1.2,
    '#parents' => array('extra', 'field_suffix'),
    '#description' => t('Text placed to the right of the paystack_payment_field'),
  );
  return $form;
}

/**
 * implements _webform_display_component()
 */
function _webform_render_paystack_payment_field($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;
  
  $element = array(
    '#type' => 'hidden',
    '#default_value' => 44,
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#prefix' => '<div class="paystack_payment_field-wrapper clearfix">hey</div>',
    '#theme_wrappers' => array('webform_element'),
  );
  if (isset($value)) {
    $element['#default_value'] = $value[0];
  }
  else {
    $element['#default_value'] = 3;
  }
  
  return $element;
}


function _webform_display_paystack_payment_field($component, $value, $format = 'html') {
  return array(
    '#title' => $component['extra']['field_prefix'] .' / '. $component['extra']['field_suffix'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_paystack_payment_field',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format' => $format,
    '#value' => $value[0],
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}

function theme_webform_display_paystack_payment_field($variables) {
  $element = $variables['element'];
  $value = $element['#value'];
 
  return trim($value) !== '' ? $value : ' ';
}