<?php

/**
 * @file Admin page for paystack donate.
 */

/**
 * Implements hook_admin().
 */
function paystack_payment_field_admin() {
  $form = array();

  $form['paystack_payment_field_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Paystack Public Key'),
    '#default_value' => variable_get('paystack_payment_field_public_key', ''),
    '#description' => t('Your pubic Key from Paystack. Use test key for test mode and live key for live mode'),
    '#required' => TRUE,
  );

  $form['paystack_payment_field_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Paystack Secret Key'),
    '#default_value' => variable_get('paystack_payment_field_secret_key', ''),
    '#description' => t('Your secret Key from Paystack. Use test key for test mode and live key for live mode'),
    '#required' => TRUE,
  );
  
  $form['paystack_payment_field_subaccount'] = array(
    '#type' => 'textfield',
    '#title' => t('Subaacount Code'),
    '#default_value' => variable_get('paystack_payment_field_subaccount', ''),
    '#description' => t('The subaccount you want to share payment with. Leave empty if you are not using the subaccount feature.'),
  );

  $form['paystack_payment_field_transaction_charge']= array(
    '#type' => 'textfield',
    '#title' => t('Subaacount Transaction Charge'),
    '#default_value' => variable_get('paystack_payment_field_transaction_charge', ''),
    '#description' => t('A flat fee to charge the subaccount for this transaction, in kobo, e.g. 7000 for a 70 naira flat fee. Please note that this value must be less than the actual amount to be paid. Leave empty if you are not using the subaccount feature.'),
    '#element_validate' => array(
      'element_validate_integer_positive',
    ),
  );

  $form['paystack_payment_field_bearer'] = array(
    '#type' => 'textfield',
    '#title' => t('Bearer'),
    '#default_value' => variable_get('paystack_payment_field_bearer', 'account'),
    '#description' => t('Who bears Paystack charges? account or subaccount . Defaults to account'),
  );

  
  

  return system_settings_form($form);
}
