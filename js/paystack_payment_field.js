function paymentButtonpaywithPaystack(id){
	var url = Drupal.settings[id].baseurl
	var uid = Drupal.settings[id].uid
	var ent_id = Drupal.settings[id].entid
	var enttype = Drupal.settings[id].enttype
	var subaccount = Drupal.settings[id].subaccount
	var sub_charge = Drupal.settings[id].subaccount_charge
	var bearer = Drupal.settings[id].bearer

	var items = {
		key: Drupal.settings[id].key,
		email: Drupal.settings[id].mail,
		amount: Drupal.settings[id].amount * 100,

		callback: function(response) {
				//alert(Drupal.settings.paystack_donate.success_message);
				(function($) {
						'use strict';
						$.redirect(url + '/payment-button/complete', { 'reference': response.reference, 'uid': uid, 'ent_id': ent_id, 'ent_type': enttype });
				}(jQuery));
		},
		onClose: function() {
				//alert('window closed');
		}
	}

	if(subaccount !== ''){
		items['subaccount'] = subaccount
		items['bearer'] = bearer
		
		if (sub_charge !== '') {
			items['transaction_charge'] = sub_charge
		}
	}

	var handler = PaystackPop.setup(items);
	handler.openIframe();
}
